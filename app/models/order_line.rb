class OrderLine < ActiveRecord::Base

  belongs_to :menu
  belongs_to :ticket

  attr_accessible :menu_id, :orderline, :ticket_id


end
