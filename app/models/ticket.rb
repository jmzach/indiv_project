class Ticket < ActiveRecord::Base
  attr_accessible :id, :ticket_complete, :ticket_number
  has_many :order_lines
  has_many :menus, through: :order_lines
end
