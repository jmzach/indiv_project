class Menu < ActiveRecord::Base
  attr_accessible :dessert, :drink, :entree, :id, :main_course, :price, :side_dish
  has_many :order_lines
  has_many :tickets, through: :order_lines
end
