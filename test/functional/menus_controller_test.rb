require 'test_helper'

class MenusControllerTest < ActionController::TestCase
  setup do
    @menu = menus(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:menus)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create menu" do
    assert_difference('Menu.count') do
      post :create, menu: { dessert: @menu.dessert, drink: @menu.drink, entree: @menu.entree, id: @menu.id, main_course: @menu.main_course, price: @menu.price, side_dish: @menu.side_dish }
    end

    assert_redirected_to menu_path(assigns(:menu))
  end

  test "should show menu" do
    get :show, id: @menu
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @menu
    assert_response :success
  end

  test "should update menu" do
    put :update, id: @menu, menu: { dessert: @menu.dessert, drink: @menu.drink, entree: @menu.entree, id: @menu.id, main_course: @menu.main_course, price: @menu.price, side_dish: @menu.side_dish }
    assert_redirected_to menu_path(assigns(:menu))
  end

  test "should destroy menu" do
    assert_difference('Menu.count', -1) do
      delete :destroy, id: @menu
    end

    assert_redirected_to menus_path
  end
end
