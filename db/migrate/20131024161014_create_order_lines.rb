class CreateOrderLines < ActiveRecord::Migration
  def change
    create_table :order_lines do |t|
      t.integer :menu_id
      t.integer :ticket_id
      t.integer :orderline

      t.timestamps
    end
  end
end
