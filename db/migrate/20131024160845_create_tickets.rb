class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :id
      t.integer :ticket_number
      t.boolean :ticket_complete

      t.timestamps
    end
  end
end
