class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.integer :id
      t.string :entree
      t.string :main_course
      t.string :side_dish
      t.string :dessert
      t.string :drink
      t.float :price

      t.timestamps
    end
  end
end
